<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apuntadoresController;
use App\Http\Controllers\MemoriaController;
use App\Http\Controllers\RecursividadController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/apuntadores', [apuntadoresController:: class, 'apuntadores']);

Route::get('/memoria', [MemoriaController:: class, 'memoria']);

Route::get('/recursividad', [RecursividadController:: class, 'incrementable']);

?>